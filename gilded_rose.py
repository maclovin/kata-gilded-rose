# -*- coding: utf-8 -*-
import bisect

MAX = 9999 #max sell_in date
REGULAR_POLICY = {'sell_in' :[0, MAX], 'action' : [-1, -1]} #default policy -> quality decrease by 1 pt

class GildedRose(object):

    def __init__(self, items, update_policy):
        self.items = []
        for item in items:
            #default behavior
            if item.name not in update_policy.keys():
                self.items.append({'item' : item,
                                'date' : REGULAR_POLICY['sell_in'],
                                'action': REGULAR_POLICY['action']})
            #custom behavior
            else:
                self.items.append({'item' : item,
                                   'date' : update_policy[item.name]['sell_in'],
                                   'action': update_policy[item.name]['action']})


    def update_quality(self):
        for item in self.items:
            date_period = bisect.bisect_left(item['date'], item['item'].sell_in)
            if date_period == -1 : date_period += 1
            item['item'].sell_in -= 1
            #quality needs to be set to 0
            if item['action'][date_period] is None: 
                item['item'].quality = 0
            else:
                if item['item'].quality < 50 and item['item'].quality > 0:
                    item['item'].quality += item['action'][date_period] 
                    #handle case where quality goes over 50, e.g. from 49 to 51
                    if(item['item'].quality) > 50 : item['item'].quality = 50

   

class Item:
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)
