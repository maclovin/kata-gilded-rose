# -*- coding: utf-8 -*-
from __future__ import print_function

from gilded_rose import *


if __name__ == "__main__":
    print ("OMGHAI!")


    items = [
             Item(name="+5 Dexterity Vest", sell_in=10, quality=20),
             Item(name="Aged Brie", sell_in=2, quality=0),
             Item(name="Elixir of the Mongoose", sell_in=5, quality=7),
             Item(name="Sulfuras, Hand of Ragnaros", sell_in=0, quality=80),
             Item(name="Sulfuras, Hand of Ragnaros", sell_in=-1, quality=80),
             Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
             Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=10, quality=49),
             Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49),
             Item(name="Conjured Mana Cake", sell_in=3, quality=6), 
             Item(name="Regular item", sell_in=3, quality=6),
            ]


  # update_policy allows new items behaviors to be declared, see data structure example below (NB both lists have to be ordered)
  # update_policy = {"Backstage passes" : {'sell_in' : [0, 5, 10, MAX], 
  #                                        'action' : [None, 2, 1, 0]}} #None means the quality will be set to 0 when the sell_in date goes under 0
  #                                                                     #2 means the quality will decrease 2* faster when sell_in < 5
                                                                        #1 means the quality will decrease by 1  when sell_in < 5
                                                                        #0 means the quality stays the same whenever sell_in > 10

    update_policy = {"Backstage passes to a TAFKAL80ETC concert" : {'sell_in' : [0, 5, 10, MAX],
                                           'action' : [None, 2, 1, 0]},
                    "Aged Brie" : {'sell_in' :[0, MAX],
                                   'action' : [1, 1]},
                    "Conjured" : {'sell_in' : [0, MAX],
                                   'action' : [-2, -2]}
                    }

    days = 2
    import sys
    if len(sys.argv) > 1:
        days = int(sys.argv[1]) + 1
    for day in range(days):
        print("-------- day %s --------" % day)
        print("name, sellIn, quality")
        for item in items:
            print(item)
        print("")
        GildedRose(items, update_policy).update_quality()